---
title: "BTI7056-DB"
subtitle: "Homework 2"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
header-includes: |
    \usepackage[]{algorithm2e}
    \usepackage{setspace}
    \doublespacing
    \renewcommand{\arraystretch}{0.7}
    \def\code#1{\texttt{#1}}
...

# Aufgabe 1
> Welche der folgenden Aussagen sind wahr? Begründen Sie jeweils, warum. Wenn eine
> Aussage falsch ist, dann zeigen Sie das mit einem Gegenbeispiel. Die Mengen $A, B,
> C$ seien Teilmengen einer beliebigen endlichen Grundmenge $U$.

## Subtask 1 (Wahr)
> Wenn $A \cup B = A \cup C$ dann $B=C$.

$$
\begin{aligned}
  A \cup B &= A \cup C \\
  \{ a \mid a \in A \lor a \in B \} &=
  \{ a \mid a \in A \lor a \in C \}
  \implies
  \{ a \mid a \in A \lor ( a \in B \land a \in C ) \} \\
  \implies
  B &= C
\end{aligned}
$$

## Subtask 2 (Wahr)
> Es gilt $A \subseteq B$ genau dann wenn $\bar{B} \subseteq \bar{A}$.

$$
\begin{aligned}
  A \subseteq B &\equiv \forall a (a \in A \implies a \in B) \\
  \bar{B} \subseteq \bar{A} &\equiv
    \forall a (a \in \bar{B} \implies a \in \bar{A}) \\
  \bar{B} \subseteq \bar{A} &\equiv
    \forall a (a \notin B \implies a \notin A) \\
  \dots ?
\end{aligned}
$$

\newpage

## Subtask 3 (Falsch)
> $A \times B=B \times A$

Gegenbeispiel [^tupel] :

[^tupel]: 1-Tupel Klammern der Kürze halber weggelassen.

$$
\begin{aligned}
  A &= \{1,2\} \\
  B &= \{3,4\} \\
  A \times B &= \{1,2\} \times \{3,4\} = \{(1,3), (1,4), (2,3), (2,4)\} \\
  B \times A &= \{3,4\} \times \{1,2\} = \{(3,1), (3,2), (4,1), (4,2)\} \\
\end{aligned}
$$

## Subtask 4 (Wahr)
> $\abs{A \times B}= \abs{A} \cdot \abs{B}$

$\mid A \times B \mid$ kann wie folgt erklärt werden:
Für jedes der $m = \mid A \mid$ Elemente in $A$ wird ein Paar mit
jedem der $n = \mid B \mid$ Elemente in $B$ gebildet.

Daraus folgt dass $\mid A \times B \mid = \mid A \mid \cdot \mid B \mid$ Elemente.

# Aufgabe 2
> Gegeben seien zwei Mengen $A$ und $B$ mit $\abs{A}=m$ und $\abs{B}=n$. Wieviele
> Relationen zwischen $A$ und $B$ gibt es? Betrachten Sie Beispiele. Begründen Sie
> Ihre Antwort.
 
Eine Relation zwischen $A$ und $B$ ist eine Untermenge von $A \times B$. Es
befinden sich $m \cdot n$ Elemente im Kartesichen Produkt $A \times B$.
Somit gibt es $2^{m \cdot n}$ Untermengen in diesem Karteischen Produkt, welche
gerade auch der Anzahl der Relationen zwischen $A$ und $B$ entspricht.


# Aufgabe 3
> Eine endliche Menge $S$ von natürlichen Zahlen kann man als Array `boolean[] s`
> repräsentieren, wie folgt: so dass gilt:
>
> ```{.java}
> s[n] = true  // falls n in S
> s[n] = false // falls n nicht in S
> ```
> 
> Die Menge $S={1,3,4}$ ist dann durch folgendes Array repräentiert:
> 
> ```{.java}
> s[0] = false
> s[1] = true
> s[2] = false
> s[3] = true
> s[4] = true.
> ```
> 
> Geben Sie möglichst einfache Algorithmen in Pseudocode an für folgende
> Funktionen:
> 
> 1. `boolean subset(boolean[] s, boolean[] t)`, die genau
> dann `true` liefert, wenn $S$ eine Teilmenge von $T$ ist.

```{.java .numberLines}
/**
 * @param s boolean[] representation of natural numbers
 * @param t boolean[] representation of natural numbers
 * @return true if s is a subset of t, otherwise false
 */
boolean isSubset(boolean[] s, boolean[] t){
    // if s contains more elements than t, it cannot be a subset
    if(s.length > t.length){
        return false;
    }

    for(int i = 0; i < s.length; i++){
        // element is in s, but IS NOT in t
        if(s[i] && !t[i]){
            return false;
        }
    }
    return true;
}
```

> 2. `boolean[] union(boolean[] s, boolean[] t)`, die die (Repräsentation der)
>   Mengenvereinigung von $S$ und $T$ liefert.

```{.java .numberLines}
/**
 * @param s boolean[] representation of natural numbers
 * @param t boolean[] representation of natural numbers
 * @return the union of both sets
 */
boolean[] union(boolean[] s, boolean[] t){
    boolean[] union = new boolean[max(s.length, t.length)];
    for(int i = 0; i < union.length; i++){
        union[i] = (get(i, s) | get(i, t));
    }
    return union;
}

/**
 * @param i desired index
 * @param arr boolean array
 * @return arr[i], when i < arr.length, otherwise false
 */
boolean get(int i, boolean[] arr){
    if(i < arr.length){
        return arr[i];
    }
    return false;
}
```

# Aufgabe 4
> Eine endliche binäre Relation $R$ auf den natürlichen Zahlen sei als ein
> zweidimensionales Array `boolean[][] r` repräsentiert, so dass gilt:
>
> ```{.java}
> r[m][n] = true  // falls (m, n) in R
> r[m][n] = false // falls (m, n) nicht in R
> ``` 
>
> Geben Sie einen möglichst einfachen Algorithmus in Pseudocode an für die
> Funktion `boolean[][] compose(boolean[][] r, boolean[][] t)`, die die
> (Repräsentation der) Komposition der Relationen $R$ und $T$ liefert.

```{.java .numberLines}
boolean[][] compose(boolean[][] r, boolean[][] t){
    boolean[][] result = new boolean[r.length][r[0].length];

    for(int m = 0; m < result.length; m++){
        for(int n = 0; n < result.length; n++){
            boolean[] row = getRow(r, m);
            boolean[] col = getCol(t, n);
            for(int i = 0; i < row.length; i++){
                result[m][n] |= (row[i] & col[i]);
            }
        }
    }

    return result;
}

boolean[] getRow(boolean[][] arr, int row){
    return arr[row];
}

boolean[] getCol(boolean[][] arr, int column){
    boolean[] result = new boolean[arr.length];
    for(int row = 0; row < arr.length; row++){
        result[row] = arr[row][column];
    }
    return result;
}
```
\newpage

Diese Lösung beruht auf Matrixmultiplikation.
Anhand eines beispiels:
$$
\{(1,2),(3,2)\} \circ \{(2,3)\} = \{(1,3),(3,3)\}
$$

In unserer `boolean[][]`-Repräsentation, sieht das ganze so aus:
$$
\begin{aligned}
R \times T &= M \\
\begin{pmatrix}
  0 & 1 & 0 \\
  0 & 0 & 0 \\
  0 & 1 & 0
\end{pmatrix}
\times
\begin{pmatrix}
  0 & 0 & 0 \\
  0 & 0 & 1 \\
  0 & 0 & 0
\end{pmatrix}
&=
\begin{pmatrix}
  0 & 0 & 1 \\
  0 & 0 & 0 \\
  0 & 0 & 1
\end{pmatrix}
\end{aligned}
$$

Um das ganze direkt auf Boolean-Types anzuwenden, und nicht auf Integer konvertieren
zu müssen, können wir Multiplikation mit boolschem-And,
und Addition mit boolschem-Or ersetzten.
Das funktioniert da $0 \equiv \code{F}$ und $1 \equiv \code{T}$ und weil
$$
\begin{aligned}
  0 \times 1 = 0 \mapsto \code{f} \land \code{t} &= \code{f} \\
  1 \times 1 = 0 \mapsto \code{t} \land \code{t} &= \code{t} \\
  0 + 1 = 1 \mapsto \code{f} \lor \code{t} &= \code{t} \\
  0 + 0 = 0 \mapsto \code{f} \lor \code{f} &= \code{f}
\end{aligned}
$$

Folgendes Konstrukt iteriert also über jeweils eine Spalte(`col[]`)
und eine Zeile(`row[]`) der Matrizen, wobei `result[m][n]`
die Ziel-Zelle in der Resultatsmatrix ist.
In jedem Schritt werden also die korrespondierenden Zellen aus der Spalte/Zeile zuerst \
*multipliziert* (`row[i] & col[i]`), und dann zum total dieser Zelle \
*addiert* (`result[m][n] |= ...`).

```{.java}
for(int i = 0; i < row.length; i++){
    result[m][n] |= (row[i] & col[i]);
}
```
Die zentrale Instruktion kann auch so notiert werden:
$$
M_{m,n} = M_{m,n} \lor (R_{m,i} \land T_{i,n})
$$

Wobei $M$ die Resultatsmatrix ist, $R,T$ die Inputmatrizen, $m,n$ die Indizes
der aktuellen Zeile und Spalte in $M$, sowie die Zeile in $R$ und und Spalte in $T$ sind.

