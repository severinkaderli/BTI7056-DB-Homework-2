#!/bin/bash
# 
# SCRIPT NAME:
# build.sh
#
# AUTHOR:
# Severin Kaderli <severin.kaderli@gmail.com>
#
# DESCRIPTION:
# This script converts a markdown file to PDF using pandoc.
#
# USAGE:
# ./build.sh FILE

# Directory of this script
SCRIPT_DIR="$( cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"

# Pandoc template file
TEMPLATE_FILE="${SCRIPT_DIR}/template.tex"

# CSL file for bibtex
CSL_FILE="${SCRIPT_DIR}/custom.csl"

# Directory where this script was executed.
# This is where the output will be saved.
START_DIR=$(pwd)

# Directory of the file
FILEDIR=$(dirname "${1}")

# Name of the input file
FILENAME=$(basename "${1}")

# The output file name
OUTPUT_FILENAME=$(echo "${FILENAME}" | cut -d "." -f -1)

# The output extension
OUTPUT_EXTENSION=".pdf"

# The current date in ISO format
CURRENT_DATE="$(date +%Y-%m-%d)"

# Enter the directory of the file to prevent issues with images
cd "${FILEDIR}" || exit 1

# Convert the file using pandoc
pandoc \
--filter pandoc-citeproc \
--csl="${CSL_FILE}" \
--from markdown+table_captions+backtick_code_blocks+footnotes+inline_notes \
--highlight-style=kate \
--toc \
--number-sections \
--template "${TEMPLATE_FILE}" \
--pdf-engine=xelatex \
--pdf-engine-opt=-shell-escape \
"${FILENAME}" \
-o "${START_DIR}/${OUTPUT_FILENAME}${OUTPUT_EXTENSION}" \
-V toc-own-page \
-V titlepage \
-V date:"${CURRENT_DATE}"

RETURN_CODE="$?"

# Return to the start directory
cd "${START_DIR}" || exit 1

exit "$RETURN_CODE"