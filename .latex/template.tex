% AUTHOR:
% Severin Kaderli <severin.kaderli@gmail.com>
%
% DESCRIPTION:
% Pandoc LaTeX template for general use in documents.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[
    $if(fontsize)$
        $fontsize$,
    $else$
        12pt,
    $endif$
    $if(papersize)$
        $papersize$,
    $else$
        a4paper,
    $endif$
    oneside,
    fleqn,
    $for(classoption)$
        $classoption$,
    $endfor$
]{scrartcl}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use needed packages
\usepackage[dvipsnames,svgnames*,table]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{csquotes}
\usepackage{physics}
\usepackage{float}
\usepackage{hyperref}
\usepackage{ifluatex}
\usepackage{ifxetex}
\usepackage{mdframed}
\usepackage{mathspec}
\usepackage{setspace}
\usepackage{siunitx}
\usepackage{titlesec}
\usepackage{titling}
\usepackage{tikz}\usepackage[european]{circuitikz}
\usepackage{pgfplots}


% Use upquote if available
\IfFileExists{upquote.sty}{
    \usepackage{upquote}
}{}

% Use microtype if available
\IfFileExists{microtype.sty}{
    \usepackage[
        $for(microtypeoptions)$
            $microtypeoptions$,
        $endfor$
    ]{microtype}
    \UseMicrotypeSet[protrusion]{basicmath}
}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Geometry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[
    margin=2.5cm,
    includehead=true,
    includefoot=true,
    centering,
    $for(geometry)$
        $geometry$,
    $endfor$
]{geometry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Captions
\definecolor{caption-color}{HTML}{777777}

% Links
\definecolor{default-linkcolor}{HTML}{A50000}
\definecolor{default-filecolor}{HTML}{A50000}
\definecolor{default-citecolor}{HTML}{4077C0}
\definecolor{default-urlcolor}{HTML}{4077C0}

% Blockquotes
\definecolor{blockquote-border}{RGB}{221,221,221}
\definecolor{blockquote-text}{RGB}{119,119,119}

% Tables
\definecolor{table-row-color}{HTML}{F5F5F5}
\definecolor{table-rule-color}{HTML}{999999}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multicolumns
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{multicol}
\newcommand{\colbegin[1]}{\begin{multicols}{#1}}
\newcommand{\colend}{\end{multicols}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Section settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Put sections on a new page
\let\oldsection\section
\renewcommand\section{\clearpage\oldsection}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paragraph settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}
\makeatletter
\renewcommand{\paragraph}{\@startsection{paragraph}{4}{0ex}%
    {-3.25ex plus -1ex minus -0.2ex}%
    {1.5ex plus 0.2ex}%
    {\normalfont\normalsize\bfseries}}
\makeatother


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Footnote settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(verbatim-in-note)$
    \usepackage{fancyvrb}
    \VerbatimFootnotes
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PDF settings
\PassOptionsToPackage{unicode=true}{hyperref}
\hypersetup{
    $if(title-meta)$
        pdftitle={$title-meta$},
    $endif$
    $if(author-meta)$
        pdfauthor={$author-meta$},
    $endif$
    $if(subject)$
        pdfsubject={$subject$},
    $endif$
    $if(keywords)$
        pdfkeywords={$for(keywords)$$keywords$$sep$, $endfor$},
    $endif$
    $if(colorlinks)$
        colorlinks=true,
        linkcolor=$if(linkcolor)$$linkcolor$$else$default-linkcolor$endif$,
        filecolor=$if(filecolor)$$filecolor$$else$default-filecolor$endif$,
        citecolor=$if(citecolor)$$citecolor$$else$default-citecolor$endif$,
        urlcolor=$if(urlcolor)$$urlcolor$$else$default-urlcolor$endif$,
    $else$
        pdfborder={0 0 0},
    $endif$
        breaklinks=true
}

% Use same font for URLs
\urlstyle{same}  

% Forcing linebreaks in URLs 
\PassOptionsToPackage{hyphens}{url}

% Make links footnotes instead of hotlinks:
$if(links-as-notes)$
    \DeclareRobustCommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Syntax highlighting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(highlighting-macros)$
    $highlighting-macros$
$endif$

\usepackage{fvextra}
\renewcommand{\theFancyVerbLine}{
    \arabic{FancyVerbLine}
}
\fvset{fontsize=\footnotesize}
\RecustomVerbatimEnvironment{verbatim}{Verbatim}{}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{
    breaklines,
    breaksymbolleft={},
    breaksymbolright={},
    commandchars=\\\{\},
    xleftmargin=1cm
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{longtable}
\usepackage{booktabs}

\renewcommand{\arraystretch}{1.75}

% Fix footnotes in tables (requires footnote package)
\IfFileExists{footnote.sty}{\usepackage{footnote}\makesavenoteenv{longtable}}{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphic settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{grffile}

\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
\setkeys{Gin}{width=0.80\linewidth,height=\maxheight,keepaspectratio}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Formatting settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(strikeout)$
    \usepackage[normalem]{ulem}
    \pdfstringdefDisableCommands{\renewcommand{\sout}{}}
$endif$

\providecommand{\tightlist}{
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}
}

$if(pagestyle)$
    \pagestyle{$pagestyle$}
$endif$

\floatplacement{figure}{H}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Section numbering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(numbersections)$
    \setcounter{secnumdepth}{3}
$else$
    \setcounter{secnumdepth}{0}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header and footer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(disable-header-and-footer)$
$else$
\usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead{}
    \fancyfoot{}
    \lhead[
        $if(header-right)$
            $header-right$
        $else$
            $date$
        $endif$
    ]{
        $if(header-left)$
            $header-left$
        $else$
            $title$
            $if(subtitle)$
                - $subtitle$
            $endif$
        $endif$
    }

    \chead[
        $if(header-center)$
            $header-center$
        $endif$
    ]{
        $if(header-center)$
            $header-center$
        $endif$
    }
    
    \rhead[
        $if(header-left)$
            $header-left$
        $else$
            $title$
        $endif$
    ]{
        $if(header-right)$
            $header-right$
        $else$
            $date$
        $endif$
    }

    \lfoot[
        $if(footer-right)$
            $footer-right$
        $else$
            \thepage
        $endif$
    ]{
        $if(footer-left)$
            $footer-left$
        $else$
            $for(author)$
                $author$$sep$,
            $endfor$
        $endif$
    }
    \cfoot[
        $if(footer-center)$
            $footer-center$
        $endif$
    ]{
        $if(footer-center)$
            $footer-center$
        $endif$
    }
    \rfoot[
        $if(footer-left)$
            $footer-left$
        $else$
            $for(author)$
                $author$$sep$,
            $endfor$
        $endif$
    ]{
        $if(footer-right)$
            $footer-right$
        $else$
            \thepage
        $endif$
    }
    
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\footrulewidth}{0.4pt}
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Blockquotes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newmdenv[
    rightline=false,
    bottomline=false,
    topline=false,
    linewidth=3pt,
    linecolor=blockquote-border,
    skipabove=\parskip
]{customblockquote}

\renewenvironment{quote}
{
    \begin{customblockquote}
    \list{}{\rightmargin=0em\leftmargin=0em}%
    \item\relax\color{blockquote-text}\ignorespaces
}
{
    \unskip\unskip\endlist\end{customblockquote}
} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Captions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[
    font={stretch=1.2},
    position=top,
    skip=4mm,
    belowskip=4mm,
    labelfont=bf,
    singlelinecheck=false,
    $if(caption-justification)$
        justification=$caption-justification$
    $else$
        justification=raggedright
    $endif$
]{caption}
\setcapindent{0em}
\captionsetup[longtable]{position=above}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\everymath{\displaystyle}

\sisetup{group-minimum-digits = 4}
\sisetup{group-separator = {'}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Language
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(lang)$
    \usepackage[
        shorthands=off,
        $for(babel-otherlangs)$
            $babel-otherlangs$,
        $endfor$
        main=$babel-lang$
    ]{babel}
$else$
    $if(mainfont)$
    $else$
        % See issue https://github.com/reutenauer/polyglossia/issues/127
        \renewcommand*\familydefault{\sfdefault}
    $endif$
    \usepackage[english]{babel}
$endif$

$if(dir)$
  \usepackage{bidi}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Meta variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{$title$}
\author{
    $for(author)$
        $author$,
    $endfor$
}
\date{$date$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header includes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$for(header-includes)$
    $header-includes$
$endfor$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Font settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{fontspec}
\usepackage{lmodern}
\renewcommand{\familydefault}{\sfdefault}

$if(linestretch)$
    \setstretch{$linestretch$}
$else$
    \setstretch{1.2}
$endif$

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Titlepage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(titlepage)$
    \pagenumbering{gobble}
    \begin{titlepage}
        \newgeometry{left=6cm}

        \newcommand{\colorRule}[3][black]{\textcolor[HTML]{#1}{\rule{#2}{#3}}}

        \begin{flushleft}
            % Set line spacing for title page
            \setstretch{1.4}

            % Text color for title page
            \color[HTML]{4F4F4F}

            % Create ruler on title page
            \makebox[0pt][l]{\colorRule[435488]{1.3\textwidth}{4pt}}

            \vskip 2em

            % Title
            {\huge \textbf{\textsf{$title$}}}
            
            % Subtitle
            $if(subtitle)$
                {\Large \textsf{$subtitle$}}
            $endif$

            \vfill

            % Authors
            $if(author)$
                \textsf{
                    \textbf{Autor:}
                    $for(author)$
                        \phantom{}\hfill $author$\newline
                    $endfor$
                }
            $endif$

            % Extra document information
            $if(extra-info)$
                $if(institute)$
                    \textsf{\textbf{Hochschule:}\hfill $institute$}
                $endif$

                $if(department)$
                    \textsf{\textbf{Abteilung:}\hfill $department$}
                $endif$

                $if(lecturer)$
                    \textsf{\textbf{Dozent:}\hfill $lecturer$}
                $endif$

                \vskip 1em
            $endif$

            % Submission information
            $if(submission)$
                \textsf{\textbf{Abgabeort:}\hfill $submission-location$}

                \textsf{\textbf{Abgabedatum:}\hfill $date$}
            $else$
                \textsf{\textbf{Datum:}\hfill $date$}
            $endif$
        \end{flushleft}
    \end{titlepage}
    \restoregeometry
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(abstract)$
    \pagenumbering{roman}
    \section*{Abstract}
    $abstract$
    \newpage
    \pagenumbering{arabic}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Include before
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$for(include-before)$
    $include-before$
$endfor$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of contents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(toc)$
    \pagenumbering{gobble}
    $if(toc-title)$
        \renewcommand*\contentsname{$toc-title$}
    $endif$

    $if(colorlinks)$
        \hypersetup{linkcolor=$if(toccolor)$$toccolor$$endif$}
    $endif$

    \setcounter{tocdepth}{$toc-depth$}

    \tableofcontents

    $if(toc-own-page)$
        \newpage
    $endif$
$endif$
\pagenumbering{arabic}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Body
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$body$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% List of figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(lof)$
    \makeatletter
    \renewcommand\listoffigures{%
        \section{\listfigurename}% Used to be \section*{\listfigurename}
        \@mkboth{\MakeUppercase\listfigurename}%
                {\MakeUppercase\listfigurename}%
        \@starttoc{lof}%
        }
    \makeatother

    \newpage
    \listoffigures
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% List of tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(lot)$
    \makeatletter
    \renewcommand\listoftables{%
    \section{\listtablename}% Used to be \section*{\listfigurename}
    \@mkboth{\MakeUppercase\listtablename}%
            {\MakeUppercase\listtablename}%
    \@starttoc{lot}%
    }
    \makeatother

    \newpage
    \listoftables
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Include after
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$for(include-after)$
    $include-after$
$endfor$


\end{document}