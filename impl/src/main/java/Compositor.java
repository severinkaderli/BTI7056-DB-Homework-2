public class Compositor {
    boolean[][] compose(boolean[][] r, boolean[][] t){
        boolean[][] result = new boolean[r.length][r[0].length];

        for(int m = 0; m < result.length; m++){
            for(int n = 0; n < result.length; n++){
                boolean[] row = getRow(r, m);
                boolean[] col = getCol(t, n);
                for(int i = 0; i < row.length; i++){
                    result[m][n] |= (row[i] & col[i]);
                }
            }
        }

        return result;
    }

    boolean[] getRow(boolean[][] arr, int row){
        return arr[row];
    }

    boolean[] getCol(boolean[][] arr, int column){
        boolean[] result = new boolean[arr.length];
        for(int row = 0; row < arr.length; row++){
            result[row] = arr[row][column];
        }
        return result;
    }
}
