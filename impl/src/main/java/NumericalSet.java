import static java.lang.Math.max;

public class NumericalSet {
    /**
     * @param s boolean[] representation of natural numbers
     * @param t boolean[] representation of natural numbers
     * @return true if s is a subset of t, otherwise false
     */
    boolean isSubset(boolean[] s, boolean[] t){
        // if s contains more elements than t, it cannot be a subset
        if(s.length > t.length){
            return false;
        }

        for(int i = 0; i < s.length; i++){
            // element is in s, but IS NOT in t
            if(s[i] && !t[i]){
                return false;
            }
        }
        return true;
    }

    /**
     * @param s boolean[] representation of natural numbers
     * @param t boolean[] representation of natural numbers
     * @return the union of both sets
     */
    boolean[] union(boolean[] s, boolean[] t){
        boolean[] union = new boolean[max(s.length, t.length)];
        for(int i = 0; i < union.length; i++){
            union[i] = (get(i, s) | get(i, t));
        }
        return union;
    }

    /**
     * @param i desired index
     * @param arr boolean array
     * @return arr[i], when i < arr.length, otherwise false
     */
    boolean get(int i, boolean[] arr){
        if(i < arr.length){
            return arr[i];
        }
        return false;
    }
}
