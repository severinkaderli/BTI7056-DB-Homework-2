import org.junit.Assert;
import org.junit.Test;

public class SubsetTest {
    private NumericalSet numericalSet = new NumericalSet();

    @Test
    public void testUnionSimple() {
        boolean[] a = new boolean[]{false, false, false, true, false}; // {3}
        boolean[] b = new boolean[]{false, true, false, true, true}; // {1,3,4}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertTrue(actual);
    }

    @Test
    public void testUnionSimpleNegative() {
        boolean[] a = new boolean[]{false, false, true, false, false}; // {2}
        boolean[] b = new boolean[]{false, true, false, true, true}; // {1,3,4}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertFalse(actual);
    }

    @Test
    public void testUnionNegative() {
        boolean[] a = new boolean[]{false, false, true, true, false}; // {2,3}
        boolean[] b = new boolean[]{false, true, false, true, true}; // {1,3,4}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertFalse(actual);
    }

    @Test
    public void testUnionDifferentLengths() {
        boolean[] a = new boolean[]{false, true}; // {1}
        boolean[] b = new boolean[]{false, true, false, true, true}; // {1,3,4}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertTrue(actual);
    }

    @Test
    public void testUnionDifferentLengthsNegative() {
        boolean[] a = new boolean[]{false, true, false, true, true}; // {1,3,4}
        boolean[] b = new boolean[]{false, true}; // {1}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertFalse(actual);
    }

    @Test
    public void testUnionEmptySet() {
        boolean[] a = new boolean[]{}; // {}
        boolean[] b = new boolean[]{false, true, false, true, true}; // {1,3,4}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertTrue(actual);
    }

    @Test
    public void testUnionBothEmptySets() {
        boolean[] a = new boolean[]{}; // {}
        boolean[] b = new boolean[]{}; // {}

        boolean actual = numericalSet.isSubset(a, b);

        Assert.assertTrue(actual);
    }
}
