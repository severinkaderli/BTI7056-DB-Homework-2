import org.junit.Assert;
import org.junit.Test;

public class UnionTest {
    private NumericalSet numericalSet = new NumericalSet();

    @Test
    public void testUnionSimple() {
        boolean[] a = new boolean[]{false, true, false, true, true}; // {1,3,4}
        boolean[] b = new boolean[]{true, true, false, true, false}; // {0,1,3}
        boolean[] expected = new boolean[]{true, true, false, true, true}; // {0,1,3,4}

        boolean[] actual = numericalSet.union(a, b);

        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testUnionDifferentLengths(){
        boolean[] a = new boolean[]{false, true, false, true, true}; // {1,3,4}
        boolean[] b = new boolean[]{true, false, true}; // {0,2}
        boolean[] expected = new boolean[]{true, true, true, true, true}; // {0,1,2,3,4}

        boolean[] actual = numericalSet.union(a, b);

        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testUnionEmptySet(){
        boolean[] a = new boolean[]{false, true, false, true, true}; // {1,3,4}
        boolean[] b = new boolean[]{}; // {}
        boolean[] expected = new boolean[]{false, true, false, true, true}; // {1,3,4}

        boolean[] actual = numericalSet.union(a, b);

        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testUnionBothEmptySets(){
        boolean[] a = new boolean[]{}; // {}
        boolean[] b = new boolean[]{}; // {}
        boolean[] expected = new boolean[]{}; // {}

        boolean[] actual = numericalSet.union(a, b);

        Assert.assertArrayEquals(expected, actual);
    }
}
