import org.junit.Assert;
import org.junit.Test;

public class CompositorTest {
    private Compositor compositor = new Compositor();

    @Test
    public void simpleCompositionTest(){
        boolean[][] r = mkData(t(1,2), t(3,2));
        boolean[][] t = mkData(t(2,3));

        boolean[][] expected = mkData(t(1,3), t(3,3));

        boolean[][] result = compositor.compose(r, t);

        assertResult(expected, result);
    }

    @Test
    public void simpleCompositionTestEmptySet(){
        boolean[][] r = mkData(t(1,2), t(3,2));
        // empty set, size checks were left out in favour of simpler code
        boolean[][] t = new boolean[4][4];

        boolean[][] expected = new boolean[4][4];

        boolean[][] result = compositor.compose(r, t);

        assertResult(expected, result);
    }

    @Test
    public void simpleCompositionTestEmptySetResult(){
        boolean[][] r = mkData(t(1,2), t(3,2));
        boolean[][] t = mkData(t(3,3));

        boolean[][] expected = new boolean[4][4];

        boolean[][] result = compositor.compose(r, t);

        assertResult(expected, result);
    }

    private void assertResult(boolean[][] expected, boolean[][] actual) {
        Assert.assertEquals("Array dimension", expected.length, actual.length);
        for(int m = 0; m < expected.length; m++){
            Assert.assertEquals("InnerArray dimension", expected[m].length, actual[m].length);
            for(int n = 0; n < expected.length; n++){
                Assert.assertEquals(String.format("arr[%d][%d]", m, n), expected[m][n], actual[m][n]);
            }
        }
    }

    private int[] t(int m, int n){
        return new int[]{m,n};
    }

    private boolean[][] mkData(int[]... raw){
        int dim = max(raw) + 1;
        boolean[][] result = new boolean[dim][dim];

        for(int[] pair : raw){
            result[pair[0]][pair[1]] = true;
        }

        return result;
    }

    private int max(int[][] arrs){
        int max = 0;
        for(int[] arr : arrs){
            for(int n : arr){
                if(n > max){
                    max = n;
                }
            }
        }
        return max;
    }
}
